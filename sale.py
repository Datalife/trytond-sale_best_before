# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    product_type = fields.Function(
        fields.Selection('get_product_types', 'Product type'),
        'on_change_with_product_type')
    best_before = fields.Date('Best before',
        states={
            'readonly': (Eval('sale_state') != 'draft'),
            'invisible': (Eval('product_type') != 'goods')
        }, depends=['sale_state', 'product_type'])

    @fields.depends('product')
    def on_change_with_product_type(self, name=None):
        if self.product:
            return self.product.type

    @classmethod
    def get_product_types(cls):
        Template = Pool().get('product.template')
        return Template.type.selection
